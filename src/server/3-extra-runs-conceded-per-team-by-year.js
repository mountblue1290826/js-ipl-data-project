const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function extraRunsConcededPerTeamByYear(year) {
  let matchesData;
  let deliveriesData;
  let resultantObj = {};

  csvToJson("../data/matches.csv")
    .then((matches) => {
      matchesData = JSON.parse(matches);
      csvToJson("../data/deliveries.csv")
        .then((deliveries) => {
          deliveriesData = JSON.parse(deliveries);

          for (let i = 0; i < matchesData.length; i++) {
            let team1ExtraRuns = 0;
            let team2ExtraRuns = 0;

            if (matchesData[i].season == year) {
              for (let j = 0; j < deliveriesData.length; j++) {
                let ball = deliveriesData[j];
                if (ball.match_id == matchesData[i].id) {
                  if (ball.bowling_team == matchesData[i].team1) {
                    team1ExtraRuns += parseInt(ball.extra_runs);
                  }
                  if (ball.bowling_team == matchesData[i].team2) {
                    team2ExtraRuns += parseInt(ball.extra_runs);
                  }
                }
              }

              if (!resultantObj[matchesData[i].team1]) {
                resultantObj[matchesData[i].team1] = team1ExtraRuns;
              } else {
                resultantObj[matchesData[i].team1] += team1ExtraRuns;
              }

              if (!resultantObj[matchesData[i].team2]) {
                resultantObj[matchesData[i].team2] = team2ExtraRuns;
              } else {
                resultantObj[matchesData[i].team2] += team2ExtraRuns;
              }
            }
          }

          const jsonString = JSON.stringify(resultantObj);
          createJsonFile(
            "../public/output/3-extraRunsConcededPerTeamByYear.json",
            jsonString
          );
        })
        .catch((error) => {
          console.log(error);
        });
    })
    .catch((error) => {
      console.log(error);
    });
}

extraRunsConcededPerTeamByYear(2016);
