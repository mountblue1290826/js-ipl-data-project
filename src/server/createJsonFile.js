const path = require("path");
const fs = require("fs");

function createJsonFile(filePath, jsonString) {
  const outputFilePath = path.join(__dirname, filePath);
  fs.writeFileSync(outputFilePath, jsonString);
}

module.exports = createJsonFile;
