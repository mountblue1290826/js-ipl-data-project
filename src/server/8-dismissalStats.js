const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function isBowlerAndPlayerPresent(resultantArray, batsman, bowler) {
    for(let i=0; i<resultantArray.length; i++){
        if(resultantArray[i].batsman==batsman && resultantArray[i].bowler==bowler){
            return i;
        }
    }
  return -1;
}

function findMostFrequentDismissalPair() {
  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      return csvToJson("../data/deliveries.csv").then((deliveries) => {
        deliveries = JSON.parse(deliveries);

        let resultantArray = [];

        for (let i = 0; i < matches.length; i++) {
          for (let delivery of deliveries) {
            if (matches[i].id == delivery.match_id) {
              let index = isBowlerAndPlayerPresent(
                resultantArray,
                delivery.batsman,
                delivery.bowler
              );
              if(index==-1){
                if(delivery.dismissal_kind && delivery.dismissal_kind != "run out"){
                    resultantArray.push({batsman: delivery.batsman, bowler: delivery.bowler, dismissal : 1})
                }
              }else{
                if(delivery.dismissal_kind && delivery.dismissal_kind != "run out"){
                    resultantArray[index].dismissal += 1;
                }
              }
            }
          }
        }

        resultantArray.sort((a, b) => b.dismissal - a.dismissal);


        const jsonString = JSON.stringify(resultantArray[0]);
        return createJsonFile(
          "../public/output/mostFrequentDismissalPair.json",
          jsonString
        );
      });
    })
    .catch((error) => {
      console.log(error.message);
    });
}

findMostFrequentDismissalPair();
