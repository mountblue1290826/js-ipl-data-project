const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function isPlayerAndSeasonPresent(resultantArray, season, player) {
  for (let i = 0; i < resultantArray.length; i++) {
    if (
      resultantArray[i].season == season &&
      resultantArray[i].batsman == player
    ) {
      return i;
    }
  }

  return -1;
}

function getStrikeRateOfBatsmenForEachSeason() {
  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      return csvToJson("../data/deliveries.csv").then((deliveries) => {
        deliveries = JSON.parse(deliveries);

        let resultantArray = [];

        for (let i = 0; i < matches.length; i++) {
          for (let delivery of deliveries) {
            if (matches[i].id == delivery.match_id) {
              let legalBall = 0;
              let legalBatsmenRuns = 0;

              if (delivery.wide_runs == 0) {
                legalBall = 1;
              }
              if (delivery.bye_runs == 0 && delivery.legbye_runs == 0) {
                legalBatsmenRuns = parseInt(delivery.batsman_runs);
              }

              let index = isPlayerAndSeasonPresent(
                resultantArray,
                matches[i].season,
                delivery.batsman
              );
              if (index == -1) {
                resultantArray.push({
                  season: matches[i].season,
                  batsman: delivery.batsman,
                  batsman_runs: legalBatsmenRuns,
                  ball_Faced: legalBall,
                  strike_rate: NaN,
                });
              } else {
                resultantArray[index].ball_Faced += legalBall;
                resultantArray[index].batsman_runs += legalBatsmenRuns;
              }
            }
          }
        }

        for (let obj of resultantArray) {
          obj.strike_rate = parseFloat(
            ((obj.batsman_runs / obj.ball_Faced) * 100).toFixed(2)
          );
        }

        resultantArray.sort((a, b) => {
          if (a.season > b.season) return -1;
          if (a.season < b.season) return 1;
          if (a.batsman_runs > b.batsman_runs) return -1;
          if (a.batsman_runs > b.batsman_runs) return 1;
          return 0;
        });

        const jsonString = JSON.stringify(resultantArray);
        return createJsonFile(
          "../public/output/7-strikeRateOfBatsmenForEachSeason.json",
          jsonString
        );
      });
    })
    .catch((error) => {
      console.log(error.message);
    });
}

getStrikeRateOfBatsmenForEachSeason();
