const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function isPresent(wonTossAsWellAsMatch, team) {
  for (let i = 0; i < wonTossAsWellAsMatch.length; i++) {
    if (wonTossAsWellAsMatch[i].team == team) {
      return i;
    }
  }
  return -1;
}

function teamsWonTossAsWellASMatch() {
  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      let wonTossAsWellAsMatch = [];

      for (let i = 0; i < matches.length; i++) {
        if (matches[i].winner) {
          let index = isPresent(wonTossAsWellAsMatch, matches[i].winner);
          let tossAndMatch = 0;

          if (matches[i].winner == matches[i].toss_winner) {
            tossAndMatch = 1;
          }
          if (index == -1) {
            wonTossAsWellAsMatch.push({
              team: matches[i].winner,
              no_times_won_toss_and_match: tossAndMatch,
            });
          } else {
            wonTossAsWellAsMatch[index].no_times_won_toss_and_match +=
              tossAndMatch;
          }
        }
      }

      const jsonString = JSON.stringify(wonTossAsWellAsMatch);
      createJsonFile(
        "../public/output/5-teamsWonTossAsWellASMatch.json",
        jsonString
      );
    })
    .catch((error) => {
      console.error("Error reading or parsing matches data:", error);
    });
}

teamsWonTossAsWellASMatch();
