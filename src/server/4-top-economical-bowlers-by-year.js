const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function bowlerPresent(resultantArray, bowler) {
  for (let i = 0; i < resultantArray.length; i++) {
    if (resultantArray[i].bowler == bowler) {
      return i;
    }
  }
  return -1;
}

function topEconomicalBowlersByYear(year, n) {
  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      csvToJson("../data/deliveries.csv")
        .then((deliveries) => {
          deliveries = JSON.parse(deliveries);

          let resultantArray = [];

          for (let match of matches) {
            if (match.season == year) {
              for (let delivery of deliveries) {
                if (delivery.match_id == match.id) {
                  let index = bowlerPresent(resultantArray, delivery.bowler);
                  let bowler_conceded_balls = 0;
                  let bowler_conceded_runs = 0;

                  // If it is a no-ball
                  if (delivery.noball_runs > 0) {
                    bowler_conceded_runs =
                      parseInt(delivery.noball_runs) +
                      parseInt(delivery.batsman_runs);
                  }
                  // If it is a wide ball
                  else if (delivery.wide_runs > 0) {
                    bowler_conceded_runs = parseInt(delivery.wide_runs);
                  }
                  // If legal delivery - (byes or leg byes are also legal deliveries)
                  else {
                    bowler_conceded_balls = 1;
                    bowler_conceded_runs = parseInt(delivery.batsman_runs);
                  }

                  if (index == -1) {
                    resultantArray.push({
                      bowler: delivery.bowler,
                      total_runs: bowler_conceded_runs,
                      total_legal_balls: bowler_conceded_balls,
                      total_overs: NaN,
                      economy: NaN,
                    });
                  } else {
                    resultantArray[index].total_runs += bowler_conceded_runs;
                    resultantArray[index].total_legal_balls +=
                      bowler_conceded_balls;
                  }
                }
              }
            }
          }

          for (let obj of resultantArray) {
            obj.total_overs = parseFloat(
              (obj.total_legal_balls / 6.0).toFixed(2)
            );
            obj.economy = parseFloat(
              ((obj.total_runs * 6.0) / obj.total_legal_balls).toFixed(2)
            );
          }

          resultantArray.sort((a, b) => a.economy - b.economy);

          let topNEconomicBowlers = [];
          for (let i = 0; i < n; i++) {
            topNEconomicBowlers.push(resultantArray[i]);
          }

          const jsonString = JSON.stringify(topNEconomicBowlers);
          createJsonFile(
            "../public/output/4-topEconomicalBowlersByYear.json",
            jsonString
          );
        })
        .catch((error) => {
          console.log(error.message);
        });
    })
    .catch((error) => {
      console.log(error.message);
    });
}

topEconomicalBowlersByYear(2015, 10);
