const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function matchesPerYear() {
  let obj = {};

  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      // Main logic
      for (let i = 0; i < matches.length; i++) {
        if (obj[matches[i].season]) {
          obj[matches[i].season]++;
        } else {
          obj[matches[i].season] = 1;
        }
      }

      const jsonString = JSON.stringify(obj);
      createJsonFile("../public/output/1-matchesPerYear.json", jsonString);
    })
    .catch((error) => {
      console.log(error.message);
    });
}

matchesPerYear();
