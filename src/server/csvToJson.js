const csv = require("csvtojson");
const path = require("path");

async function csvToJson(relativePath) {
  try {
    const csvFilePath = path.join(__dirname, relativePath);
    const jsonArray = await csv().fromFile(csvFilePath);
    const jsonResult = JSON.stringify(jsonArray, null, 4);
    return jsonResult;
  } catch (error) {
    console.error("Error converting CSV to JSON:", error);
  }
}

// Example usage
// const csvFilePath = path.join(__dirname, '../data/matches.csv');
// csvToJson(csvFilePath);

module.exports = csvToJson;
