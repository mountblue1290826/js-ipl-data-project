const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function getTopPlayerOfTheMatchPerSeason() {
  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      let playersStats = {};

      for (let i = 0; i < matches.length; i++) {
        if (!playersStats[matches[i].season]) {
          playersStats[matches[i].season] = {};
        }
        if (!playersStats[matches[i].season][matches[i].player_of_match]) {
          playersStats[matches[i].season][matches[i].player_of_match] = 0;
        }
        playersStats[matches[i].season][matches[i].player_of_match]++;
      }

      let topPlayerOfTheMatchPerSeason = [];

      for (let season in playersStats) {
        let maximumWin = 0;
        let playerName = "";
        for (let player in playersStats[season]) {
          if (playersStats[season][player] > maximumWin) {
            maximumWin = playersStats[season][player];
            playerName = player;
          }
        }
        topPlayerOfTheMatchPerSeason.push({
          season,
          name: playerName,
          win: maximumWin,
        });
      }

      const jsonString = JSON.stringify(topPlayerOfTheMatchPerSeason);
      createJsonFile(
        "../public/output/6-topPlayerOfTheMatchPerSeason.json",
        jsonString
      );
    })
    .catch((error) => {
      console.error("Error reading or parsing matches data:", error);
    });
}

getTopPlayerOfTheMatchPerSeason();
