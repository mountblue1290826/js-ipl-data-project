const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

function matchesWonPerTeamPerYear() {
  let obj = {};

  csvToJson("../data/matches.csv")
    .then((matches) => {
      matches = JSON.parse(matches);

      for (let i = 0; i < matches.length; i++) {
        if (!obj[matches[i].season]) {
          obj[matches[i].season] = {};
        }

        if (matches[i].winner) {
          if (obj[matches[i].season][matches[i].winner]) {
            obj[matches[i].season][matches[i].winner]++;
          } else {
            obj[matches[i].season][matches[i].winner] = 1;
          }
        }
      }

      const jsonString = JSON.stringify(obj);
      createJsonFile(
        "../public/output/2-matchesWonPerTeamPerYear.json",
        jsonString
      );
    })
    .catch((error) => {
      console.log(error.message);
    });
}

matchesWonPerTeamPerYear();
