const csvToJson = require("./csvToJson.js");
const createJsonFile = require("./createJsonFile.js");

async function mostSixesByPlayerBySeason() {
  let matches = await csvToJson("../data/matches.csv");
  matches = JSON.parse(matches);
  let deliveries = await csvToJson("../data/deliveries.csv");
  deliveries = JSON.parse(deliveries);

  let sixesByPlayerBySeason = {};

  for (let match of matches) {
    for (let delivery of deliveries) {
      if (match.id == delivery.match_id) {
        if (delivery.batsman_runs == 6) {
          if (!sixesByPlayerBySeason[match.season]) {
            sixesByPlayerBySeason[match.season] = {};
          }
          sixesByPlayerBySeason[match.season][delivery.batsman] =
            (sixesByPlayerBySeason[match.season][delivery.batsman] || 0) + 1;
        }
      }
    }
  }

  let mostSixesByPlayer = {};

  for (let season of Object.keys(sixesByPlayerBySeason)) {
    let batsmanName = [];
    let sixes = 0;
    for (let batsman of Object.keys(sixesByPlayerBySeason[season])) {
      if (sixesByPlayerBySeason[season][batsman] > sixes) {
        sixes = sixesByPlayerBySeason[season][batsman];
      }
    }
    for (let batsman of Object.keys(sixesByPlayerBySeason[season])) {
      if (sixesByPlayerBySeason[season][batsman] == sixes) {
        batsmanName.push({
          batsman: batsman,
          sixes: sixesByPlayerBySeason[season][batsman],
        });
      }
    }
    mostSixesByPlayer[season] = batsmanName;
  }

  const jsonString = JSON.stringify(mostSixesByPlayer);
  createJsonFile(
    "../public/output/10-mostSixsByPlayerBySeason.json",
    jsonString
  );
}

mostSixesByPlayerBySeason();
